
package exercices;

import java.util.Scanner;


public class exO1 {

  
    public static void main(String[] args) {
        //Déclaration d'un dispositif de saisie via le clavier
        Scanner clavier=new Scanner(System.in);
        // Déclaration des variables en entrée
        //Largeur et longueur de type float
        // c'est à dire nombre a virgule
        float largeur,longueur;
    
        //Déclaration des variables de sortie
        //qui contiendront les résusltats
        // Règles d'écriture (camel)
        //un nom de cariable doit commencer par une minuscule
        //pas d'accent dans les noms de variables
   
        float perimetre, surface;
       
        //Affichage d'une question
        System.out.println("Largeur ?");
        //acquisition d'un nombre à virgule via le clavier 
        //enregistrement das la varioable largeur
        largeur= clavier.nextFloat();
        
        //Affichage d'une question
        System.out.println("Longueur ?");
        //acquisition d'un nombre à virgule via le clavier 
        //enregistrement das la varioable longeur
        longueur= clavier.nextFloat();
        //Calcul et affectation dans les variables
        //perimetre et surface
        perimetre=2*(longueur+largeur);
        surface=longueur*largeur;
        //Affichage des résultats
        System.out.println("Périmètre: "+perimetre);
        System.out.println("Surface: "+surface);
        
    
    
    
    
    
    }
}
